/**
 * 
 * Create spirals inside shape
 * 
 * author : Laurent Malys (lo@crans.org)
 * 
 */

/* import toxi.geom.*; */
/* import toxi.geom.mesh2d.*; */

/* import toxi.util.*; */
/* import toxi.util.datatypes.*; */

/* import toxi.processing.*; */

Vec2D line_intersect(Vec2D v1, Vec2D v2, Vec2D v3, Vec2D v4){
  
  // Line2D l=new Line2D(v1, v2);
  // Line2D m=new Line2D(v3, v4);
  // Line2D.LineIntersection isec=l.intersectLine(m);
  
  // if (isec.getType()==Line2D.LineIntersection.Type.INTERSECTING) {
  //   return isec.getPos();
  // }else{
  //   return new Vec2D(0, 0);
  // }
  
    float x1 = v1.x;
    float x2 = v2.x;
    float x3 = v3.x;
    float x4 = v4.x;
    
    float y1 = v1.y;
    float y2 = v2.y;
    float y3 = v3.y;
    float y4 = v4.y;
    
    float nom_x = (x1*y2 - y1*x2)*(x3 - x4) - (x1 - x2)*(x3*y4 - y3*x4);
    float denom = (x1 - x2)*(y3 - y4) - (y1 - y2)*(x3 - x4);
    float nom_y = (x1*y2 - y1*x2)*(y3 - y4) - (y1 - y2)*(x3*y4 - y3*x4);

    float x = nom_x / denom;
    float y = nom_y / denom;

    return new Vec2D(x, y);
}

class SpPoint{
  float angle = 0;
  Vec2D dir = new Vec2D(0, 0);
  Vec2D pt = new Vec2D(0,0);
  Vec2D edge = new Vec2D(0,0);
  int num = 0;

  SpPoint(Vec2D pt0, Vec2D pt1, Vec2D center, int num_in){
    num = num_in;
    pt = pt0;
    dir = center.sub(pt0).normalize();
    edge = pt1.sub(pt).normalize();
    angle = dir.angleBetween(edge);
  }
}

class Spiral{
  float w;
  Polygon2D pts_in;
  SpPoint[] pts;
  Polygon2D spiral;
  Vec2D center;

  Line2D next_segment = new Line2D(new Vec2D(0, 0), new Vec2D(0,0));
  
  ArrayList<SpPoint> cur_pts;
  ArrayList<Line2D> lines_debug;
  ArrayList<Vec2D> points_debug;

  ArrayList<Line2D> segments;

  Vec2D last_n;
  Vec2D last_last_n;
  boolean ok = true;
  int i_boucle = 0;
  int i_pt;
  float cur_w;

  Vec2D cur_p;
  Vec2D cur_dir;

  int cpt = 0;

  Vec2D pt0;

  float space = 1;
  int n_pt_to_draw;

  Spiral(Polygon2D points, float w_in){


    segments = new ArrayList<Line2D>();
    center = points.getCentroid();
    pts_in = points;
    Vec2D tt = points.vertices.get(0);
    int n_points = points.getNumPoints();
    
    cur_pts = new ArrayList<SpPoint>();

    pts = new SpPoint[n_points];
    for (int i = 0; i < n_points; i++){
      segments.add(new Line2D(points.vertices.get(i), points.vertices.get((i+1)%n_points)));

      pts[i] = new SpPoint(points.vertices.get(i), points.vertices.get((i+1)%n_points), center, i);
      cur_pts.add(pts[i]);
    }
    
    spiral = new Polygon2D();
    w = w_in;

    cur_w = w -  w / cur_pts.size();
    last_n = pts[0].pt;
    last_last_n = pts[0].pt;
    lines_debug = new ArrayList<Line2D>();
    points_debug = new ArrayList<Vec2D>();

    i_pt = 0;
    // float max_y = 0;
    // for (int i = 0; i < points.vertices.size(); i ++){
    //   if (points.vertices.get(i).y > max_y){
    //     i_pt = (i-1);
    //     if (i_pt == -1){
    //       i_pt = points.vertices.size()-1;
    //     }
    //     max_y = points.vertices.get(i).y;
    //   }
    // }
    // points_debug.add(points.vertices.get(i_pt));
  }
  
  Vec2D nextIntersec(int i_plus, Vec2D p, Vec2D m){
    //    float cur_w1 = cur_w + i_plus * w / cur_pts.size();
    float cur_w1 = cur_w + i_plus * w / (cur_pts.size()-1);
    SpPoint cur_p1 = cur_pts.get((i_pt+i_plus)%cur_pts.size());
    float l1 = cur_w1 / sin(cur_p1.angle);
    
    Vec2D p1 = cur_p1.pt.add(cur_p1.dir.scale(l1));
    Vec2D m1 = p1.add(cur_p1.edge);
    
    return line_intersect(p, m, p1, m1);
  }
  
  void setPt0(int i_pt_0){
    i_pt_0 = max(0, i_pt_0);
    points_debug.add(new Vec2D(cur_pts.get(i_pt_0).pt.x,cur_pts.get(i_pt_0).pt.y));
    i_pt = i_pt_0-1;
    if (i_pt < 0){
      i_pt = cur_pts.size()-1;
    }
  }
  
  void setSpace(float space_in){
    space = space_in;
    n_pt_to_draw = floor(map(space, 0.0f, 1.0f, 0.0f, spiral.getNumPoints()));
  }

  void create_sp(){
    println("create sp");
    float min_dist = 100000;
    for (SpPoint pt: pts){
      float c_dist = center.distanceTo(pt.pt);
      min_dist = min(min_dist, c_dist);
    }
    float rad_max = min_dist;
    float rad = 0;
    float a = 0;
    float step_a = 0.01;
    float step_rad = 0.01;

    println(rad_max);
    while (rad < rad_max){
      Vec2D cur_p = new Vec2D(rad*cos(a), rad*sin(a));
      cur_p = cur_p.add(center);
      rad += step_rad;
      a += step_a;
      spiral.add(cur_p);
    }
    println(space);
  }

  void create_alt(){
    int i_pt_init = i_pt;
    SpPoint pt_0 = cur_pts.get(i_pt);
    float l = w / sin(pt_0.angle);
    cur_p = pt_0.pt.add(pt_0.dir.scale(l));
    cur_dir = pt_0.edge;
    while (!pts_in.containsPoint(cur_p)){
      i_pt = (i_pt+1)%cur_pts.size();
      pt_0 = cur_pts.get(i_pt); 
      if (i_pt == i_pt_init){
        break;
      }
      pt_0 = cur_pts.get(i_pt);
      l = w / sin(pt_0.angle);
      cur_p = pt_0.pt.add(pt_0.dir.scale(l));
      cur_dir = pt_0.edge;
    }
    points_debug.add(cur_p);
    spiral.add(cur_p);
    //lines_debug.add(new Line2D(cur_p, cur_p.add(cur_dir.scale(100))));
    while (ok){
    createStep_alt();
     }
  }


  void createStep_alt(){
    if (ok){
      // lines_debug = new ArrayList<Line2D>();
      //   // points_debug = new ArrayList<Vec2D>();
      Line2D line_cut = new Line2D(cur_p, cur_p.add(cur_dir.scale(1000)));
      
      float min_d = 10000;
      Line2D min_seg = new Line2D(new Vec2D(0,0), new Vec2D(0,0));
      
      Vec2D inter = new Vec2D(0, 0);
      for (Line2D l: segments){
        Line2D.LineIntersection isec = line_cut.intersectLine(l);
        if (isec.getType()==Line2D.LineIntersection.Type.INTERSECTING) {
          Vec2D pos = isec.getPos();
          float d = pos.distanceTo(cur_p);
          if (d < min_d && d > w){
            min_d = d;
            min_seg = l;
            inter = pos;
          }
        }
      }
      segments.add(next_segment);
      Vec2D next_p = new Vec2D(0,0);
      if (min_d < 10000){
        segments.remove(min_seg);
        points_debug.add(inter);
        Vec2D next_dir = min_seg.getDirection();
        float an = PI - cur_dir.angleBetween(next_dir);
        float delta = w / sin(an);
        if ((min_d - delta) < 0){
          print(min_d - delta);
          println(" stop");

          ok = false;
        }
        //       lines_debug.add(new Line2D(cur_p, cur_p.add(next_dir.scale(100))));
        next_p = cur_p.add(cur_dir.normalize().scale(min_d-delta ));
        // println(an*180/PI);
        //points_debug.add(next_p);
        next_segment = new Line2D(cur_p.add(cur_dir.scale(-1000)), next_p);
        //lines_debug.add(new Line2D(cur_p, next_p));

        //        println(next_dir.cross(cur_dir));
        lines_debug = new ArrayList<Line2D>();
        //lines_debug.add(new Line2D(next_p, next_p.add(next_dir.normalize().scale(100))));
        //lines_debug.add(new Line2D(next_p, next_p.add(cur_dir.normalize().scale(100))));
        println(min_d);
        
        if (next_dir.cross(cur_dir) > 0){
          ok = false;
        }
        cur_p = next_p;
        cur_dir = next_dir;
        
      }else{
        ok = false;
      }
      
      if (!pts_in.containsPoint(cur_p)){
        ok = false;
      }

      if (min_d < w){
        ok = false;
      }
      
      if (ok){
        lines_debug.add(new Line2D(cur_p, next_p));
        spiral.add(cur_p);
      }
      
      cpt++;
      // if (cpt > 2){
      //   ok = false;
      // }
      //  Vec2D dir_n = n.sub(last_n);
      //   //lines_debug.add(new Line2D(n, last_n));
      Vec2D last_dir_n = last_n.sub(last_last_n);
      
      //   print(cpt);
      //   print(" ");
      //   print(dir_n);
      //   print(" ");
      //   print(last_dir_n);
      //   print(" ");
      //   print(last_dir_n.cross(dir_n));
      //   print(" ");
      //   println(dir_n.dot(cur_p.edge));
      // if ((ok) && (last_dir_n.cross(dir_n) < 0) && (cpt > 1)){
      //   //lines_debug.add(new Line2D(cur_p.pt, cur_p.edge));
      //   ok = false;
      // }//else{
      //     if (ok){
      //       points_debug.add(n);
     //       spiral.add(n);
     //       i_pt = (i_pt+1)%cur_pts.size();
     //       if (i_pt == 0){
     //         i_boucle += 1;
     //       }
     //     } 
     //   }
       //     last_last_n = last_n.copy();
       //last_n = n.copy();
     //   if (i_boucle > 50){
     //     ok = false;
     //   }
     //   if (cur_pts.size() <= 2){
     //     ok = false;
     //   }
     //   cpt ++;
    }
  }

  void createStep(){
    if (ok){
      lines_debug = new ArrayList<Line2D>();
       // points_debug = new ArrayList<Vec2D>();

       cur_w = cur_w + w / (cur_pts.size()-1);

       SpPoint cur_p = cur_pts.get(i_pt);
       float l = cur_w / sin(cur_p.angle);
       Vec2D p = cur_p.pt.add(cur_p.dir.scale(l));
       Vec2D m = p.add(cur_p.edge);

       Vec2D n = nextIntersec(1, p, m);
       Vec2D o = nextIntersec(2, p, m);

       //points_debug.add(n);
       //points_debug.add(o);

       Vec2D dir_o = o.sub(last_n);

       boolean change = false;

       if(last_n.distanceTo(o) < last_n.distanceTo(n)){
         change = true;
       }

       if (!pts_in.containsPoint(o)){
         change = false;
       }

       if (o.distanceTo(n) > 3*w){
         change = false;
       }

       if (cur_pts.size() <= 3){
         change = false;
       }

       if (change){
         //cur_w += w/cur_pts.size();
         n = o;
         if (i_pt + 1 == cur_pts.size()){
           cur_pts.remove(0);
           i_pt -= 1;
         }else{
           cur_pts.remove(i_pt + 1);
         }
       }

       Vec2D dir_n = n.sub(last_n);
       //lines_debug.add(new Line2D(n, last_n));
       Vec2D last_dir_n = last_n.sub(last_last_n);
       
       if (!pts_in.containsPoint(n)){
         ok = false;
       }

       /* print(cpt); */
       /* print(" "); */
       /* print(dir_n); */
       /* print(" "); */
       /* print(last_dir_n); */
       /* print(" "); */
       /* print(last_dir_n.cross(dir_n)); */
       /* print(" "); */
       /* println(dir_n.dot(cur_p.edge)); */
       if ((ok) && (last_dir_n.cross(dir_n) < 0) && (cpt > 4)){
         //lines_debug.add(new Line2D(cur_p.pt, cur_p.edge));
         ok = false;
       }else{
         if (ok){
           points_debug.add(n);
           spiral.add(n);
           i_pt = (i_pt+1)%cur_pts.size();
           if (i_pt == 0){
             i_boucle += 1;
           }
         } 
       }
       last_last_n = last_n.copy();
       last_n = n.copy();
       if (i_boucle > 50){
         ok = false;
       }
       if (cur_pts.size() <= 2){
         ok = false;
       }
       cpt ++;
     }
   }

   void create(){
    while (ok){
      createStep();
    }
  }
  
  void draw(){
    beginShape();
    //    for (Vec2D pt: spiral.vertices){
    
    //for (int i = 0; i < n_pt_to_draw; i++){
    for (int i = 0; i < spiral.getNumPoints()*space; i++){
      Vec2D pt = spiral.vertices.get(i);
      vertex(pt.x, pt.y);
    }
    endShape();
    //draw_debug();
  }
  
  void fix(){
    int n_points = spiral.getNumPoints();
    for (int ii = 1; ii < n_points-2; ii++){
      int i = n_points - ii;
      Line2D l1 = new Line2D(spiral.vertices.get(i), spiral.vertices.get(i-1));
      for (int jj = 2; jj < i-1; jj++){
        int j = i - jj;
        Line2D l2 = new Line2D(spiral.vertices.get(j), spiral.vertices.get(j-1));
        Line2D.LineIntersection isec = l1.intersectLine(l2);
        if (isec.getType()==Line2D.LineIntersection.Type.INTERSECTING){
          //Polygon2D.add(spiral.vertices.get(i));
          if (i < spiral.getNumPoints()){
            spiral.vertices.remove(i);
          }
        }
      }
    }
    
    spiral.flipVertexOrder();
    n_pt_to_draw = spiral.getNumPoints();
  }
  
  void draw_debug(){
    pushStyle();
    fill(255, 0, 0);
    stroke(255, 0, 0);
    for (int i = 0; i < lines_debug.size(); i++){
      gfx.line(lines_debug.get(i));
    }

    for (int i = 0; i < points_debug.size(); i++){
      ellipse(points_debug.get(i).x, points_debug.get(i).y, 5, 5);
    }
    popStyle();
  }
}
