class Region{
  JSONObject region_json;
  JSONObject params;
  VerletPhysics2D physics;
  VerletParticle2D center;

  int pop;
  int surf;
  float x;
  float y;

  // input parameters
  FloatRange xpos, ypos, rep_range;

  float rep_force;
  float rep_range_delta;
  float min_rep_force_range;
  float max_rep_force_range;
  float att_range;
  float min_att_force;
  float max_att_force;

  float att_force;
  float rep_force_range;  

  ArrayList<VerletParticle2D> points;
  ArrayList<AttractionBehavior2D> center_attractions;
  ArrayList<AttractionBehavior2D> points_repulsions;

  Region(JSONObject region_json_in, VerletPhysics2D physics_in, JSONObject params){
    region_json = region_json_in;
    physics = physics_in;
    
    pop = region_json.getInt("Pop");
    surf = region_json.getInt("surf");
    x = map(region_json.getInt("x"), -100, 800, 0, width);
    //float x = map(a.getInt("x"), 0, 800, 0, width);
    y = map(region_json.getInt("y"), 0, 700, 0, height);
    
    rep_force = params.getFloat("rep_force");
    rep_range_delta = params.getFloat("rep_range_delta");
    
    min_rep_force_range = params.getFloat("min_rep_force_range");
    max_rep_force_range = params.getFloat("max_rep_force_range");
    
    att_range = params.getFloat("att_range");
    min_att_force = params.getFloat("min_att_force");
    max_att_force = params.getFloat("max_att_force");

    xpos = new FloatRange(-10, 10);
    ypos = new FloatRange(-10, 10);
    rep_range = new FloatRange(-rep_range_delta, rep_range_delta);

    att_force = map(log(pop/surf), log(14), log(6000), min_att_force, max_att_force); 
    rep_force_range = map(log(surf), log(43000), log(500), max_rep_force_range, min_rep_force_range);

    points = new ArrayList<VerletParticle2D>();    
    center_attractions = new ArrayList<AttractionBehavior2D>();
    points_repulsions = new ArrayList<AttractionBehavior2D>();
  }
  
  void addCenter(){
    center = new VerletParticle2D(new Vec2D(x, y));
    physics.addParticle(center);
    center.lock();
  }
  
  VerletParticle2D addPoint(){
    
    VerletParticle2D p = new VerletParticle2D(new Vec2D(x + xpos.pickRandom(), 
                                                        y + ypos.pickRandom()));
    
    AttractionBehavior2D center_att = new AttractionBehavior2D(center, att_range, att_force, 0.01f);
    p.addBehavior(center_att);
    center_attractions.add(center_att);                  
    
    physics.addParticle(p);
    AttractionBehavior2D point_rep = new AttractionBehavior2D(p, 
                                                              rep_force_range + rep_range.pickRandom(),
                                                              -rep_force, 
                                                              0.01f);
    physics.addBehavior(point_rep);
    points_repulsions.add(point_rep);
    
    return p; 
  }

  void setRepForce(float min_rep_range, float max_rep_range){
    rep_force_range = map(log(surf), log(43000), log(500), max_rep_force_range, min_rep_force_range);
    for (AttractionBehavior2D a: points_repulsions){
      a.setRadius(rep_force_range);
    }
  }
  
  void setAttForce(float min_att, float max_att){
    att_force = map(log(pop/surf), log(14), log(6000), min_att, max_att); 
    for (AttractionBehavior2D a: center_attractions){
      a.setStrength(att_force);
    }
  }

  void setAttRadius(float att_radius){
    att_range = att_radius;
    for (AttractionBehavior2D a: center_attractions){
      a.setRadius(att_range);
    }
  }
}
