class Cell implements Comparable<Cell>{
  int id;
  Polygon2D poly;
  VerletParticle2D part;
  Spiral spiral;
  boolean spiral_done;
  JSONObject region;
  float larg;
  boolean debug = false;  
  ArrayList<Integer> ids_node;
  ArrayList<NodeCell> node_cells;
  int id_tresse;
  int i_pt0;
  boolean has_tresse;
  NodeCell tresse;
  float space = 0;

  float area = -1;
  
  public Cell(JSONObject region_in, VerletParticle2D part_in){
    region = region_in;
    part = part_in;
    
    i_pt0 = 0;
    id_tresse = 0;
    has_tresse = false;
    tresse = new NodeCell(new Vec2D(0, 0));
    ids_node = new ArrayList<Integer>();
    node_cells = new ArrayList<NodeCell>();
  }

  public Cell(JSONObject json_in){
    setFromJson(json_in);
  }

  void setFromJson(JSONObject json_in){
    JSONArray lst_pts = json_in.getJSONArray("pts");
    poly = new Polygon2D();
    for (int i = 0; i < lst_pts.size(); i++){
      JSONObject json_pt = lst_pts.getJSONObject(i);
      poly.add(new Vec2D(json_pt.getFloat("x"), json_pt.getFloat("y")));
    } 
    i_pt0 = json_in.getInt("i_pt0");
    region = json_in.getJSONObject("region");

    tresse = new NodeCell(new Vec2D(0, 0));
    ids_node = new ArrayList<Integer>();
    node_cells = new ArrayList<NodeCell>();
  }
  
  JSONObject getAsJson(){
    JSONObject json_out = new JSONObject();
    JSONArray lst_pts = new JSONArray();
    for (Vec2D v: poly.vertices){
      JSONObject pt = new JSONObject();
      pt.setFloat("x", v.x);
      pt.setFloat("y", v.y);
      lst_pts.append(pt);
    }
    json_out.setJSONArray("pts", lst_pts);
    json_out.setJSONObject("region", region);
    json_out.setInt("i_pt0", i_pt0);

    return json_out;
  }

  void setPoly(Polygon2D poly_in, float larg_in){
    poly = poly_in;
    larg = larg_in;
  }

  void setSpace(float space_in){
    space = space_in;
  }

  void setTresse(int id_tresse_in){
    id_tresse = id_tresse_in;
    i_pt0 = ids_node.indexOf(id_tresse);
    has_tresse = true;
  }

  void setTresseNode(NodeCell n){
    tresse = n;
    i_pt0 = node_cells.indexOf(n);
    has_tresse = true;
  }

  void makeSpiral(){
    //float l = map(region.getFloat("surf"), 550, 42000, 5, 15);  
    //float s = map(region.getFloat("Pop"), 100000, 3600000, 0.5, 1);  
    spiral = new Spiral(poly, larg);
    spiral.setPt0(i_pt0);
    spiral.create_sp();
    spiral.setSpace(space);
    spiral.fix();
    spiral_done = true;
  }

  void addIdNode(int id_in){
    ids_node.add(id_in);
  }

  void addNodeCell(NodeCell nc){
    node_cells.add(nc);
  }

  float getArea(){
    if (area <0){
      area = abs(poly.getArea());
    }
    return area;
  }

  int getPop(){
    return region.getInt("Pop");
  }

  int getSurf(){
    return region.getInt("surf");
  }

  Polygon2D getPoly(){
    return poly;
  }
  
  Vec2D getPos(){
    return new Vec2D(part.x, part.y);
  }

  ArrayList<Integer> getIdNode(){
    return ids_node;
  }

  void draw(){
    if (debug){
      if (region.getString("name").equals("DAKAR")){
        pushStyle();
        stroke(0, 255, 0);
        ellipse(part.x, part.y, 10, 10);
        //spiral.draw();
        popStyle();
      }else{
        ellipse(part.x, part.y, 10, 10);
      }
    }
    
    if (spiral_done){
      // if (region.getString("name").equals("DAKAR")){
      //   pushStyle();
      //   stroke(0, 255, 0);
      //   spiral.draw();
      //   popStyle();
      // }else{
        spiral.draw();
        //} 
    }
  }

  void reInit(){
    i_pt0 = 0;
    id_tresse = 0;
    has_tresse = false;
    tresse = new NodeCell(new Vec2D(0, 0));
    ids_node = new ArrayList<Integer>();
    node_cells = new ArrayList<NodeCell>(); 
  }

  @Override     
  public int compareTo(Cell cell_b) {
    float a = poly.getCentroid().x;
    float b = cell_b.poly.getCentroid().x;
    if (a > b){
      return -1;
    }else if(a < b){
      return 1;
    }else{
      return 0;
    }
  }
}
