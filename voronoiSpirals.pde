/**
 * This demo shows the basic usage pattern of the Voronoi class in combination with
 * the ConvexPolygonClipper to constrain the resulting shapes within an octagon boundary.
 *
 * Usage:
 * mouse click: add point to voronoi
 * p: toggle points
 * t: toggle triangles
 * x: clear all
 * r: add random
 * c: toggle clipping
 * h: toggle help display
 * space: save frame
 *
 * Voronoi class ported from original code by L. Paul Chew
 */

/* 
 * Copyright (c) 2010 Karsten Schmidt
 * 
 * This demo & library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * http://creativecommons.org/licenses/LGPL/2.1/
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
import toxi.geom.*;
import toxi.geom.mesh2d.*;

import toxi.physics2d.*;
import toxi.physics2d.behaviors.*;

import toxi.util.*;
import toxi.util.datatypes.*;

import toxi.processing.*;

import processing.pdf.*;
import processing.svg.*;

import controlP5.*;

JSONObject json;
JSONObject params;

// helper class for rendering
ToxiclibsSupport gfx;

// empty voronoi mesh container
Voronoi voronoi = new Voronoi();
boolean voronoi_done = false;

// ranges for x/y positions of points
FloatRange xpos, ypos, rep_range;

// switches
boolean doShowPoints = true;
boolean doShowDelaunay;
boolean doShowHelp=true;

boolean mode_voronoi = false;
boolean mode_spiral = false;
boolean doSave;
boolean saveOneFrame = false;
// particles
int NUM_PARTICLES = 500;

VerletPhysics2D physics;
AttractionBehavior2D mouseAttractor;
AttractionBehavior2D centerAttractor;

Vec2D mousePos;

// Param
int hab_p_cell;
float rep_force;
float rep_range_delta;
float min_rep_force_range;
float max_rep_force_range;
float att_range;
float min_att_force;
float max_att_force;
float largeur_bride;

float border_density = 25;
float border_radius = 70;
float mouse_radius = 200;
float mouse_voronoi_radius = 120;

// regions objects
ArrayList<Region> regions;

// clip
Rect clip;
SutherlandHodgemanClipper clipper;

// cells
ArrayList<Cell> cells;
ArrayList<NodeCell> node_cells;

// gui
ControlP5 cp5;

float bride_min = 2.0;
float bride_max = 12.0;

float bride_lim_min = 4.0;
float bride_lim_max = 12.0;

int bride_map = 0;
ScrollableList bride_map_list;

float space_min = 0.5;
float space_max = 1.0;

float space_lim_min = 0.0;
float space_lim_max = 1.0;
Range space_range;
int space_range_y;

// area
float min_area;
float max_area;

// debug
ArrayList<Line2D> lines_debug;
ArrayList<Vec2D> points_debug;
int n_cells_debug = 0;

String f_json;


void setup() {
  fullScreen();
  //  size(1920, 1080);
  smooth();
  lines_debug = new ArrayList<Line2D>();
  points_debug = new ArrayList<Vec2D>();
 
  params = loadJSONObject("param.json");
  // focus x positions around horizontal center (w/ 33% standard deviation)
  xpos=new FloatRange(-10, 10);
  // focus y positions around bottom (w/ 50% standard deviation)
  ypos=new FloatRange(-10, 10);

  f_json = params.getString("load_json");
  
  hab_p_cell = params.getInt("hab_p_cell");

  rep_force = params.getFloat("rep_force");
  rep_range_delta = params.getFloat("rep_range_delta");

  min_rep_force_range = params.getFloat("min_rep_force_range");
  max_rep_force_range = params.getFloat("max_rep_force_range");

  att_range = params.getFloat("att_range");
  min_att_force = params.getFloat("min_att_force");
  max_att_force = params.getFloat("max_att_force");

  bride_min = params.getFloat("bride_min");
  bride_max = params.getFloat("bride_max");

  bride_lim_min = params.getFloat("bride_lim_min");
  bride_lim_max = params.getFloat("bride_lim_max");

  space_min = params.getFloat("space_min");
  space_max = params.getFloat("space_max");

  space_lim_min = params.getFloat("space_lim_min");
  space_lim_max = params.getFloat("space_lim_max");

  border_density = params.getFloat("border_density");
  border_radius = params.getFloat("border_radius");
  mouse_radius = params.getFloat("mouse_radius");
  mouse_voronoi_radius = params.getFloat("mouse_voronoi_radius");
  bride_map = params.getInt("bride_map");
  
  rep_range = new FloatRange(-rep_range_delta, rep_range_delta);
  
  // gui
  cp5 = new ControlP5(this);
  
  //  cp5.setColorValue(0x0000ff00);
  
  int cp5_y = 20;
  cp5.addRange("rep_force_range", 0, 300, min_rep_force_range, max_rep_force_range, 20, cp5_y, 200, 20)
    .setLabel("Repulsion rayon");

  cp5_y += 30;
  cp5.addRange("att_force_range", 0, 2, min_att_force, max_att_force, 20, cp5_y, 200, 20)
    .setLabel("Attraction force");

  cp5_y += 30;
  cp5.addSlider("att_radius", 0, 2000, att_range, 20, cp5_y, 200, 20)
    .setLabel("Attraction rayon");

  cp5_y += 50;
  cp5.addRange("bride_min_max",0,30,bride_min,bride_max, 20, cp5_y,200,20)
    .setLabel("Largeur brides min/max mapping").setColorValueLabel(0x0000ff);

  cp5_y += 30;  
  cp5.addRange("bride_limites",0,30,bride_lim_min,bride_lim_max, 20, cp5_y, 200,20)
    .setLabel("Largeur brides min/max limites");

  cp5_y += 30;
  java.util.List l = java.util.Arrays.asList("Taille des cellules", "Population", "Surface");
  bride_map_list = cp5.addScrollableList("bride_map")
    .setPosition(20, cp5_y)
    .setSize(200, 100)
    .setBarHeight(20)
    .setItemHeight(20)
    .addItems(l)
    // .setType(ScrollableList.LIST) // currently supported DROPDOWN and LIST
    ;
  
  bride_map_list.setValue(bride_map);
  
  cp5_y += 110;
  cp5.addRange("space_min_max",0,1.0,space_min, space_max, 20, cp5_y,200,20)
    .setLabel("min/max space");

  cp5_y += 30;
  space_range = cp5.addRange("space_limites",0,1.0,space_lim_min, space_lim_max, 20, cp5_y,200,20)
    .setLabel("limites space");
  space_range_y = cp5_y;

  max_area = 0;
  min_area = 1000000000;

  // init toxiclibs
  gfx = new ToxiclibsSupport(this);
  textFont(createFont("SansSerif", 10));

  clip = new Rect(0, 0, width, height);
  clipper = new SutherlandHodgemanClipper(clip);
  
  // setup physics
  physics = new VerletPhysics2D();
  physics.setDrag(0.1f);
  physics.setWorldBounds(new Rect(0, 0, width, height));
  
  // init cells
  cells = new ArrayList<Cell>();
  node_cells = new ArrayList<NodeCell>();

  regions = new ArrayList<Region>();
  
  if (!f_json.equals("")){ 
    loadCells();
  }else{
    initCells();
  }
}

void initCells(){
  // get regions data
  json = loadJSONObject("data.json");
  
  JSONArray toto = json.getJSONArray("regions");
  
  float delta_density = width/border_density;
  for (int x = 0; x < width; x += delta_density){
    VerletParticle2D p = new VerletParticle2D(x, 0);
    physics.addParticle(p);
    physics.addBehavior(new AttractionBehavior2D(p, 
                                                 border_radius,
                                                 -rep_force, 
                                                 0.01f));
    p.lock();
    p = new VerletParticle2D(x, height);
    physics.addParticle(p);
    physics.addBehavior(new AttractionBehavior2D(p, 
                                                 border_radius,
                                                 -rep_force, 
                                                 0.01f));
    p.lock();
  }

  for (int y = 0; y < height; y += delta_density){
    VerletParticle2D p = new VerletParticle2D(0, y);
    physics.addParticle(p);
    physics.addBehavior(new AttractionBehavior2D(p, 
                                                 border_radius,
                                                 -rep_force, 
                                                 0.01f));
    p.lock();
    p = new VerletParticle2D(width, y);
    physics.addParticle(p);
    physics.addBehavior(new AttractionBehavior2D(p, 
                                                 border_radius,
                                                 -rep_force, 
                                                 0.01f));
    p.lock();
  }

  for (int i = 0; i < toto.size(); i++){
    JSONObject a = toto.getJSONObject(i);
    println(a.getString("name"));
    Region new_region = new Region(a, physics, params);
    int pop = a.getInt("Pop");
    
    new_region.addCenter();
    for (int j = 0; j < pop; j+=hab_p_cell){
      VerletParticle2D p = new_region.addPoint();
      cells.add(new Cell(a, p));
    }
    regions.add(new_region);
  }
  
  mousePos = new Vec2D(mouseX, mouseY);
  if (mouse_radius > 0){
    mouseAttractor = new AttractionBehavior2D(mousePos, mouse_radius, -1.0f);
    physics.addBehavior(mouseAttractor);  
  }
}

void draw() {
  background(200);
  stroke(0);
  noFill();

  if (!mode_spiral && !mode_voronoi){
    physics.update();
    voronoi_done = false;
  }
  
  if (doShowPoints){  
    for (VerletParticle2D p : physics.particles) {
      ellipse(p.x, p.y, 5, 5);
    }
  }  

  if (mode_spiral){
    
    if(saveOneFrame == true) {
      beginRecord(SVG, "spirals" + DateUtils.timeStamp() + ".svg"); 
    }
    for (Cell c : cells){
      c.draw();
    }
    
    if(saveOneFrame == true) {
      endRecord();
      saveOneFrame = false; 
    }      
  }
  
  if (mode_voronoi){
    if (mode_spiral){
      
    //   if(saveOneFrame == true) {
    //     beginRecord(SVG, "spirals.svg"); 
    //   }
    //   for (Cell c : cells){
    //     c.draw();
    //   }
      
    //   if(saveOneFrame == true) {
    //     endRecord();
    //     saveOneFrame = false; 
    //   }      
    }else{
      if (!voronoi_done){
        voronoi = new Voronoi();
        
        for (Cell c: cells){
          c.reInit();
          voronoi.addPoint(c.part);
        }
        if (mouse_radius > 0){
          voronoi.addPoint(mousePos);
          for (int i = 0; i < 10; i++){
            float r = mouse_voronoi_radius;
            voronoi.addPoint(mousePos.add(new Vec2D(r*cos(i*2*PI/10), r*sin(i*2*PI/10))));
          }
        }
        voronoi_done = true;

        java.util.List<Polygon2D> regions = voronoi.getRegions();

        for (int i_cell = 0 ; i_cell < cells.size() ; i_cell++){
          Cell c = cells.get(i_cell);
          Polygon2D poly = new Polygon2D();
          //poly = regions.get(i_cell);
          for (Polygon2D r: voronoi.getRegions()){
            if (r.containsPoint(c.part)){
              poly = r;
              break;
            }
          }
          
          poly = clipper.clipPolygon(poly);
            
          float cur_area = abs(poly.getArea());
          
          if (cur_area > 10){
            max_area = max(cur_area, max_area);
            min_area = min(cur_area, min_area);
          }            

          if (!poly.isClockwise()){
            poly = poly.flipVertexOrder();
          }
          
          float larg = map(abs(cur_area),min_area, max_area, 4.0, 13.0);
          float space = map(abs(cur_area),min_area, max_area, 1.0, 0.5);
          
          c.setPoly(poly, larg);
          c.setSpace(space);
        }

        space_range = cp5.addRange("space_limites",min_area, max_area, space_lim_min, space_lim_max,20,space_range_y,200,20).setLabel("limites space");
          

        java.util.Collections.sort(cells);
        node_cells = getNodeCells();
        
        for (Cell c: cells){
          c.setTresseNode(getTresse(c));
          c.makeSpiral();
        }
        
        // DEBUG
        // println("num cell tresses");
        // for (NodeCell c: node_cells){
        //   if (c.getIsTresse()){
        //     println(c.getNumTresseCells());
        //   }
        // }

      }
      
    }
    
    if (!mode_spiral){
      println(min_area);
      println(max_area);
      for (Polygon2D poly : voronoi.getRegions()) {
        gfx.polygon2D(poly);
      }

      // for (Cell c: cells){
      //   c.draw();
      // }

      for (NodeCell c: node_cells){
        if (c.getIsTresse()){
          ellipse(c.getPos().x, c.getPos().y, 15, 15);
        }
      }
      
      pushStyle();
      stroke(0,0, 255);
      for (Line2D l: lines_debug){
        gfx.line(l);
      }

      // for (Vec2D p: points_debug ){
      //   ellipse(p.x, p.y, 10, 10);
      // }

      for (NodeCell nc: cells.get(n_cells_debug).node_cells){
        ellipse(nc.pos.x, nc.pos.y, 10, 10);
      }
      
      popStyle();

    }
    // draw delaunay triangulation
    if (doShowDelaunay) {
      stroke(0, 0, 255, 50);
      beginShape(TRIANGLES);
      for (Triangle2D t : voronoi.getTriangles()) {
        gfx.triangle(t, false);
      }
      endShape();
    }
    // draw original points added to voronoi
    if (doShowPoints) {
      fill(255, 0, 255);
      noStroke();
      for (Vec2D c : voronoi.getSites()) {
        ellipse(c.x, c.y, 5, 5);
      }
    }
  }
  
  if (doSave) {
    saveParams("params-" + DateUtils.timeStamp() + ".json");
    saveFrame("voronoi-" + DateUtils.timeStamp() + ".png");
    if (mode_voronoi){
      saveCells("cells-" + DateUtils.timeStamp() + ".json");
    }
    doSave = false;
  }
  
  if (doShowHelp) {
    fill(255, 0, 0);
    text("p: toggle points", 20, 20);
    text("t: toggle triangles", 20, 40);
    text("v: toggle voronoi", 20, 60);
    text("r: toggle spirals", 20, 80);
    text("h: toggle help display", 20, 100);
    text("space: save frame", 20, 120);
  }
}

void keyPressed() {
  switch(key) {
  case ' ':
    doSave = true;
    saveOneFrame = true;
    break;
  case 't':
    doShowDelaunay = !doShowDelaunay;
    break;
  case 'v':
    mode_voronoi = !mode_voronoi;
    // for (VerletParticle2D p : physics.particles) {
    //   p.unlock();
    // }
    break;
  case 'x':
    voronoi = new Voronoi();
     for (VerletParticle2D p : physics.particles) {
       voronoi.addPoint(new Vec2D(p.x, p.y));
     }
    break;
  case 'p':
    doShowPoints = !doShowPoints;
    break;
  case 'h':
    doShowHelp=!doShowHelp;
    break;
  case 'r':
    for (int i = 0; i < 10; i++) {
      voronoi.addPoint(new Vec2D(xpos.pickRandom(), ypos.pickRandom()));
    }
    break;
  case 's':
    mode_spiral = !mode_spiral;
    if (mode_spiral){
      doShowPoints = false;
      doShowHelp = false;
    }
    break;

  case 'd':
    n_cells_debug ++;
    //    voronoi_done = false;
    lines_debug = new ArrayList<Line2D>();
    points_debug = new ArrayList<Vec2D>();
 
    break;
  }
}

void addParticle(int x, int y) {
  VerletParticle2D p = new VerletParticle2D(new Vec2D(x, y));
  //voronoi.addPoint(new Vec2D(mouseX, mouseY));
  
  p.addBehavior(new AttractionBehavior2D(new Vec2D(x, y), 1000, 0.1f, 0.01f));
  physics.addParticle(p);
  // add a negative attraction force field around the new particle
  physics.addBehavior(new AttractionBehavior2D(p, rep_range.pickRandom(), -rep_force, 0.01f));
}

void mousePressed() {
  // VerletParticle2D p = new VerletParticle2D(new Vec2D(mouseX, mouseY));
  // //voronoi.addPoint(new Vec2D(mouseX, mouseY));
  
  // physics.addParticle(p);
  // // add a negative attraction force field around the new particle
  // physics.addBehavior(new AttractionBehavior2D(p, 20, -1.0f, 0.2f));
}

void mouseMoved() {
  if (!mode_voronoi && !mode_spiral){
    mousePos.set(mouseX, mouseY);
  }
}

ArrayList<NodeCell> getNodeCells(){
  int cptcpt = 0;
  ArrayList<NodeCell> nodes = new ArrayList<NodeCell>();
  for (Cell c: cells){
    for (Vec2D v: c.getPoly().vertices){
      boolean add_to_list = true;
      cptcpt ++;
      for (NodeCell nc: nodes){
        if (nc.getPos().distanceTo(v) < 0.1){
          nc.addCell(c);
          c.addIdNode(nodes.indexOf(nc));
          c.addNodeCell(nc);
          add_to_list = false;
        }
      }
      if (add_to_list){
        NodeCell new_nc = new NodeCell(v);
        new_nc.addCell(c);
        c.addNodeCell(new_nc);
        nodes.add(new_nc);
      }
    }
  }
  
  // java.util.Collections.sort(nodes);

  // for (NodeCell n: nodes){
  //   if (n.cells.size() >= 3){
  //     boolean tr = true;
  //     for (Cell c: n.cells){
  //       if (c.has_tresse){
  //         tr = false;
  //       }
  //     }
  //     if (tr){
  //       for (Cell c: n.cells){
  //         c.setTresseNode(n);
  //       }
  //       println("tresse");
  //     }
  //   }
  // }

  // for (NodeCell n: nodes){
  //   if (n.cells.size() >= 3){
  //     boolean tr = true;
  //     for (Cell c: n.cells){
  //       if (c.has_tresse){
  //         tr = false;
  //       }
  //     }
  //     if (tr){
  //       for (Cell c: n.cells){
  //         c.setTresseNode(n);
  //       }
  //       println("tresse");
  //     }
  //   }
  // }


  return nodes;
}


NodeCell getTresse(Cell c){
  NodeCell n_out = new NodeCell(new Vec2D(0,0));
  boolean old_tresse_exists = false;
  for (NodeCell n: c.node_cells){
    int max_tresse = 0;
    if (n.is_tresse){
      if (n.getNumTresseCells()>max_tresse){
        n_out = n;
        max_tresse = n.getNumTresseCells();
        old_tresse_exists = true;
      }
    }
  }
  
  if (!old_tresse_exists){
    for (NodeCell n : c.node_cells){
      if (n.getNumCells() >= 3){
        if (!n.getIsNearTresse()){
          n_out = n;
        }
      }
    }
  }

  for (NodeCell n: c.node_cells){
    n.setIsNearTresse(true);
  }
  n_out.addTresseCell(c);

  return n_out;
}

NodeCell getFarTresse(Cell c){
  ArrayList<NodeCell> tresse_node = new ArrayList<NodeCell>();
  float dist_min = 100000;
  NodeCell farest_vertex = new NodeCell(new Vec2D(0,0));
  NodeCell closest_tresse = new NodeCell(new Vec2D(0,0));
  
  for (NodeCell nc : node_cells){
    if (nc.getIsTresse()){
      tresse_node.add(nc);
      float d = nc.getPos().distanceTo(c.getPos()); 
      if ( d < dist_min){
        dist_min = d; 
        closest_tresse = nc;
      }
    }
  }
  
  if (dist_min == 100000){
    for (NodeCell n: c.node_cells){
      if (n.getNumCells()>=3){
        return n;
      }
    }
  }

  float dist_max = 0;
  
  for (NodeCell n: c.node_cells){
    float d = n.pos.distanceTo(closest_tresse.getPos());
    if (d > dist_max){
      //      points_debug.add(node_cells.get(i_cell).pos);
      if (n.getNumCells() >= 3){
        if (!n.getIsNearTresse()){
          dist_max = d;
          farest_vertex = n;
        }
      }
    }
  }
  
  lines_debug.add(new Line2D(farest_vertex.getPos(), closest_tresse.getPos()));
  return farest_vertex;
}

void bride_min(float l_min){
  bride_min = l_min;;
  if (mode_spiral){
    for (Cell c: cells){
      float larg = map(abs(c.poly.getArea()),min_area, max_area, bride_min, bride_max);
      float space = map(abs(c.poly.getArea()),min_area, max_area, 1.0, 0.5);
      c.setPoly(c.poly, larg);
      c.setSpace(space);
      c.makeSpiral();
    }  
  }
} 

void bride_max(float l_max){
  bride_max = l_max;
  if (mode_spiral){
    for (Cell c: cells){
      float larg = map(abs(c.poly.getArea()),min_area, max_area, bride_min, bride_max);
      float space = map(abs(c.poly.getArea()),min_area, max_area, 1.0, 0.5);
      c.setPoly(c.poly, larg);
      c.setSpace(space);
      c.makeSpiral();
    }  
  }
} 

void space_min(float l_min){
  space_min = l_min;
  if (mode_spiral){
    for (Cell c: cells){
      float space = map(abs(c.poly.getArea()),min_area, max_area, space_max, l_min);
      c.setSpace(space);
      c.makeSpiral();
    }  
  }
} 

void space_max(float l_max){
  space_max = l_max;
  majSpirals();
} 


void controlEvent(ControlEvent theControlEvent) {
  if (theControlEvent.isFrom("rep_force_range")){
    min_rep_force_range = theControlEvent.getController().getArrayValue(0);
    max_rep_force_range = theControlEvent.getController().getArrayValue(1);
    for (Region r: regions){
      r.setRepForce(min_rep_force_range, max_rep_force_range);
    }
 
  }else if(theControlEvent.isFrom("att_force_range")){
    min_att_force = theControlEvent.getController().getArrayValue(0);
    max_att_force = theControlEvent.getController().getArrayValue(1);
    for (Region r: regions){
      r.setAttForce(min_att_force, max_att_force);
    }
 
  }else if(theControlEvent.isFrom("att_radius")){
    att_range = (int)theControlEvent.getController().getValue();
    println(att_range);
    for (Region r: regions){
      r.setAttRadius(att_range);
    }
 
  }else if(theControlEvent.isFrom("bride_limites")) {
    bride_lim_min = theControlEvent.getController().getArrayValue(0);
    bride_lim_max = theControlEvent.getController().getArrayValue(1);
  }else if (theControlEvent.isFrom("bride_min_max")) {
    bride_min = theControlEvent.getController().getArrayValue(0);
    bride_max = theControlEvent.getController().getArrayValue(1);
  }else if(theControlEvent.isFrom("space_limites")) {
    space_lim_min = theControlEvent.getController().getArrayValue(0);
    space_lim_max = theControlEvent.getController().getArrayValue(1);
  }else if (theControlEvent.isFrom("space_min_max")) {
    space_min = theControlEvent.getController().getArrayValue(0);
    space_max = theControlEvent.getController().getArrayValue(1);
  }else if (theControlEvent.isFrom("bride_map")) {
    //    bride_map = theControlEvent.getController().getStringValue();
    bride_map = (int)theControlEvent.getController().getValue();
  }

  if (mode_spiral){
    majSpirals();
  }
}


void bride_limites(float l_min, float l_max){
  bride_lim_min = l_min;
  bride_lim_max = l_max;
}

void saveParams(String f_out){
  JSONObject params_out = new JSONObject();
  
  params_out.setString("load_json", f_json);
  
  params_out.setInt("hab_p_cell", hab_p_cell);

  params_out.setFloat("rep_force", rep_force);
  params_out.setFloat("rep_range_delta", rep_range_delta);

  params_out.setFloat("min_rep_force_range", min_rep_force_range);
  params_out.setFloat("max_rep_force_range", max_rep_force_range);

  params_out.setFloat("att_range", att_range);
  params_out.setFloat("min_att_force", min_att_force);
  params_out.setFloat("max_att_force", max_att_force);

  params_out.setFloat("bride_min", bride_min);
  params_out.setFloat("bride_max", bride_max);

  params_out.setFloat("bride_lim_min", bride_lim_min);
  params_out.setFloat("bride_lim_max", bride_lim_max);

  params_out.setFloat("space_min", space_min);
  params_out.setFloat("space_max", space_max);

  params_out.setFloat("space_lim_min", space_lim_min);
  params_out.setFloat("space_lim_max", space_lim_max);

  params_out.setFloat("border_density", border_density);
  params_out.setFloat("border_radius", border_radius);
  params_out.setFloat("mouse_radius", mouse_radius);
  params_out.setFloat("mouse_voronoi_radius", mouse_voronoi_radius);

  params_out.setInt("bride_map", bride_map);

  saveJSONObject(params_out, f_out);
}

void saveCells(String f_out){
  JSONArray json_out = new JSONArray();
  for(Cell c: cells){
    json_out.append(c.getAsJson());
  }
  saveJSONArray(json_out, f_out);
}

void loadCells(){
  JSONArray json_in = loadJSONArray("cells.json");
  cells = new ArrayList<Cell>();
  for (int i = 0; i < json_in.size(); i++){
    Cell c = new Cell(json_in.getJSONObject(i));
    cells.add(c);    
    
    float cur_area = abs(c.poly.getArea());
    
    if (cur_area > 10){
      max_area = max(cur_area, max_area);
      min_area = min(cur_area, min_area);
    }
  }
  
  space_range = cp5.addRange("space_limites",min_area, max_area, space_lim_min, space_lim_max,20, space_range_y,200,20).setLabel("limites space");
  
  mode_spiral = true;
  majSpirals();
}

void majSpirals(){
  if (mode_spiral){
    for (Cell c: cells){
      float larg = 0.0;
      if (bride_map == 1){
        int pop = c.getPop();
        larg = map(log(pop), log(111207), log(3215255), bride_max, bride_min);
      }else if (bride_map == 2){
        int surf = c.getSurf();
        larg = map(surf, 547, 42363, bride_min, bride_max);
      }else{
        larg = map(abs(c.poly.getArea()),min_area, max_area, bride_min, bride_max);
      }
      larg = min(larg, bride_lim_max);
      larg = max(larg, bride_lim_min);
      
      int pop = c.getPop();
      float space = map(log(pop), log(111207), log(3215255), space_min, space_max);
      //      float space = map(abs(c.poly.getArea()),space_lim_min, space_lim_max, space_max, space_min);
      space = min(space, 1.0);
      space = max(space, 0.0);

      c.setPoly(c.poly, larg);
      c.setSpace(space);
      c.makeSpiral();
    }  
  }
  println(min_area);
}
