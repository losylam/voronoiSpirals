class NodeCell implements Comparable<NodeCell>{
  ArrayList<Integer> ids_cell;
  ArrayList<Integer> ids_tresse_cell;
  ArrayList<Cell> cells;
  ArrayList<Cell> tresse_cells;
  Vec2D pos;
  boolean is_tresse = false;
  boolean is_near_tresse = false;

  NodeCell(Vec2D pos_in){
    pos = pos_in;
    ids_cell = new ArrayList<Integer>();
    ids_tresse_cell = new ArrayList<Integer>();
    cells = new ArrayList<Cell>();
    tresse_cells = new ArrayList<Cell>();
  }

  void addCellId(int id_cell){
    ids_cell.add(id_cell);
  }

  void addCell(Cell c){
    cells.add(c);
  }

  void addTresseCellId(int id_cell){
    ids_tresse_cell.add(id_cell);
  }

  void addTresseCell(Cell c){
    is_tresse = true;
    tresse_cells.add(c);
  }

  int getNumCells(){
    return ids_cell.size();
  }
  
  int getNumTresseCells(){
    return tresse_cells.size();
  }

  Vec2D getPos(){
    return pos;
  }
  
  void setIsTresse(boolean is_tresse_in){
    is_tresse = is_tresse_in;
  }

  void setIsNearTresse(boolean is_tresse_in){
    is_near_tresse = is_tresse_in;
  }

  boolean getIsTresse(){
    return is_tresse;
  }

  boolean getIsNearTresse(){
    return is_near_tresse;
  }

  @Override     
    public int compareTo(NodeCell cell_b) {
    float a = pos.x;
    float b = cell_b.pos.x;
    if (a > b){
      return 1;
    }else if(a < b){
      return -1;
    }else{
      return 0;
    }
  }
}
